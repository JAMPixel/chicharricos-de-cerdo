#include "balle.h"
#include "fichier.h"

Element * init_balle(){
  Element * balle = createImage(Xi-Rayon,Yi-Rayon,Rayon,Rayon,"Skin_balle/lv1.png",0,1);
  dballe_t * dballe = (dballe_t *) malloc(sizeof(dballe_t));
  dballe->rayon = Rayon;
  dballe->vx = 2;
  dballe->vy = -3;
  dballe->run=1;

  for(int j=0;j<NbC;j++){
    for(int i=0;i<NbL;i++){
      dballe->tbloc[i][j]=NULL;
    }
  }

  setDataElement(balle, (void *) dballe);
  setActionElement(balle, deplacement);

  return balle;
}

// Delta de déplacement

void msg_perdu(){
  int blanc[] = {255, 255, 255, 0};
  Element * msg_perdu = createText(W_Window*0.25, H_Window*0.25, W_Window*0.5, H_Window*0.25, W_Window*0.25, "comicbd.ttf","Perdu, try again !", blanc, SANDAL2_BLENDED, 0, 0);
  //updateWindow();
  //displayWindow();
  //SDL_Delay(600);
}

// perdu + close jeu si sortie de l'écran
void sortie(Element * balle){
  float x, y,w,h;
  getCoordElement(balle, &x, &y);
  getDimensionElement(balle,&w,&h);
  dballe_t * structBalle;
  getDataElement(balle,(void **)&structBalle);
  if (x<0 || x+w>W_Window || y<0 || y+h>H_Window){
    msg_perdu();
    structBalle->run=0;
  }

}

void deplacement(Element * balle){
  dballe_t * structBalle;
  getDataElement(balle,(void **)&structBalle);
  collisions(balle);
  moveElement(balle,structBalle->vx,structBalle->vy);
  sortie(balle);
}
