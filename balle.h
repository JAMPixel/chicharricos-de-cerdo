#ifndef __BALLE__
#define __BALLE__

#include "structure.h"
/*
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
*/

// Vitesse en x, y (coordonnées en getcoord)
// Tableau des blocs
/*
// Data de la balle
typedef struct dballe{
  int rayon;
  int vx, vy;
  Element ** tbloc;
}dballe_t;
*/

Element * init_balle();
void deplacement(Element * balle);
void sortie(Element * balle);

#endif
