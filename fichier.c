#include "structure.h"
#include "fichier.h"

void TabBlock(FILE * fic, Element * balle){
  int temp=0;
  //int red[]={255,0,0,255};
  structBlock_t * tempBlock;
  dballe_t * structBalle;
  getDataElement(balle,(void **)&structBalle);
  if(fic){
      for(int j=0;j<NbC;j++){
        for(int i=0;i<NbL;i++){
        fscanf(fic,"%d",&temp);
        tempBlock=(structBlock_t *)malloc(sizeof(structBlock_t));
        switch(temp) {
          case 3:
          case 1:
          case 2:
            tempBlock->type=3;
            if(i==0 || i==NbL-1 || j==0 || j==NbC-1)
              (structBalle->tbloc)[i][j]=createImage((i+0.125)*Lb,(j+0.125)*Lb,0.75*Lb,0.75*Lb,"Skin_balle/bloc_partiel.png",0,0);
            else
              (structBalle->tbloc)[i][j]=createImage((i+0.25)*Lb,(j+0.25)*Lb,0.5*Lb,0.5*Lb,"Skin_balle/bloc_partiel.png",0,0);
            setDataElement((structBalle->tbloc)[i][j],(void *)tempBlock);
            break;
          default :
            (structBalle->tbloc)[i][j]=NULL;
        }
      }
    }
  }
}

void collisions(Element * balle){
  dballe_t * structBalle;
  int iballe,jballe,zoneCollision=0;
  float xballe,yballe,xblock,yblock,hballe,wballe,hblock,wblock;

  getCoordElement(balle,&xballe,&yballe);
  getDimensionElement(balle,&wballe,&hballe);

  getIJBalle(balle,&iballe,&jballe);
  getDataElement(balle,(void **)&structBalle);

  for(int j=0;j<NbC;j++){
    for(int i=0;i<NbL;i++){
      if((structBalle->tbloc)[i][j]){
        getCoordElement((structBalle->tbloc)[i][j],&xblock,&yblock);
        getDimensionElement(structBalle->tbloc[i][j],&wblock,&hblock);
        zoneCollision=getCoteBlock(balle,structBalle->tbloc[i][j]);
        //printf("xballe %f yballe %f wballe %f hballe %f iballe %d jballe %d i %d j %d xblock %f yblock %f wblock %f hblock %f cote %d\n",xballe,yballe,wballe,hballe,iballe,jballe,i,j,xblock,yblock,wblock,hblock,zoneCollision);
        switch(zoneCollision){
          case 1:
          if(yballe+hballe>=yblock){
            structBalle->vy=(-1)*(structBalle->vy);
            //replaceElement(balle,xballe,yballe-10);
            //printf("y+h %f >= y %f\n",yballe+hballe,yblock);
            //puts("collision");
          }
          break;
          case 2:
          if(yballe<=yblock+hblock){
            structBalle->vy=(-1)*(structBalle->vy);
            //replaceElement(balle,xballe,yballe+10);
            //printf("y %f <= y+h %f\n",yballe,yblock+hblock);
            //puts("collision");
          }
          break;
          case 3:
          if(xballe+wballe>=xblock){
            structBalle->vx=(-1)*(structBalle->vx);
            //replaceElement(balle,xballe-10,yballe);
            //printf("x+w %f >= y %f\n",xballe+wballe,xblock);
            //puts("collision");
          }
          break;
          case 4:
          if(xballe<=xblock+wblock){
            structBalle->vx=(-1)*(structBalle->vx);
            //replaceElement(balle,xballe+10,yballe);
            //printf("x %f <= x+w %f\n",xballe,xblock+wblock);
            //puts("collision");
          }
          break;
        }

      }
    }
  }
  //printf("------------------------------------------\n");
}

void getIJBalle(Element * balle,int * i,int * j){
  float x,y,w,h,xmil,ymil;
  (*i)=0;
  (*j)=0;
  getCoordElement(balle,&x,&y);
  getDimensionElement(balle,&w,&h);
  xmil=x+w*0.5;
  ymil=y+h*0.5;
  while(xmil>=Lb){
    xmil=xmil-Lb;
    (*i)++;
  }
  while(ymil>=Lb){
    ymil=ymil-Lb;
    (*j)++;
  }
}

int getCoteBlock(Element * balle,Element * block){
  float xballe,yballe,xblock,yblock,hballe,wballe,hblock,wblock,xmilieu,ymilieu;
  int res=0;

  getCoordElement(balle,&xballe,&yballe);
  getDimensionElement(balle,&wballe,&hballe);
  getCoordElement(block,&xblock,&yblock);
  getDimensionElement(block,&wblock,&hblock);

  xmilieu=xballe+wballe*0.5;
  ymilieu=yballe+hballe*0.5;

  if(xmilieu>=xblock-20 && xmilieu<=xblock+wblock+20){
    if(ymilieu<yblock+yblock*0.5){
      res=1;
    }else{
      res=2;
    }
  }else if(ymilieu>=yblock-20 && ymilieu<=yblock+hblock+20){
    if(xmilieu<xblock+xblock*0.5){
      res=3;
    }else{
      res=4;
    }
  }
  return res;
}

void clearTab(Element * balle){
  dballe_t * structBalle;
  getDataElement(balle,(void **)&structBalle);
  for(int j=0;j<NbC;j++){
    for(int i=0;i<NbL;i++){
      if((structBalle->tbloc)[i][j]){
        delElement((structBalle->tbloc)[i][j]);
        (structBalle->tbloc)[i][j]=NULL;
      }
    }
  }
}
