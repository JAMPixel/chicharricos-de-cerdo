#include "izziredoublement.h"


int usb_open(char const * filename) {

	struct termios options;
	int fd = open(filename, O_RDWR|O_NOCTTY);
	if(fd < 0) return fd;

	fcntl(fd, F_SETFL, 0);
	tcgetattr(fd, &options);
	usleep(10000);
	cfsetospeed(&options, B9600);
	cfsetispeed(&options, B9600);
	options.c_cflag &= ~PARENB;
	options.c_cflag &= ~CSTOPB;
	options.c_cflag &= ~CSIZE;
	options.c_cflag |= CS8;
	options.c_iflag &= ~(IGNBRK|BRKINT|PARMRK|ISTRIP|INLCR|IGNCR|ICRNL|IXON);
	options.c_oflag &= ~OPOST;
	options.c_lflag &= ~(ICANON|ECHO|ECHONL|IEXTEN|ISIG);

	options.c_cc[VMIN] = 1;
	options.c_cc[VTIME] = 40;
	tcsetattr(fd, TCSANOW, &options);
	tcflush(fd, TCIOFLUSH);
	usleep(10000);

	return fd;
}

int creationMatrice(int fd1,int fd2,FILE * fichier)
{
	char s[N];
	char c;
	int i;
	int j;
	char d;
	int b=1;
  FILE * nucleo1;
  FILE * nucleo2;



	if(fichier){
		if ((fd1>0)&&(fd2>0))
		{
			nucleo1 = fdopen(fd1, "r");
			nucleo2 = fdopen(fd2, "r");

			if ((nucleo1)&&(nucleo2))
			{
				while(b){
					d=fgetc(nucleo1);
					if (d=='d')
					{
						b=0;
					}
				}
				fgets(s,NBligne*NBcolone*2,nucleo1);
				for (i = 0; i <NBligne ; i++)
				{
					for (j = 0; j <NBcolone ; j++)
					{
						c=s[2*j+i*NBcolone*2];
						fprintf(fichier,"%c ",c);
					}
					fprintf(fichier, "\n");
				}
				b=1;
				while(b){
					d=fgetc(nucleo2);
					if (d=='d')
					{
						b=0;
					}
				}
				fgets(s,NBligne*NBcolone*2,nucleo2);
				for (i = 0; i <NBligne ; i++)
				{
					for (j = 0; j <NBcolone ; j++)
					{
						c=s[2*j+i*NBcolone*2];
						fprintf(fichier,"%c ",c);
					}
					fprintf(fichier, "\n");
				}


			}
			fflush(nucleo1);
			fflush(nucleo2);
		}

	}

 return 0;
}
