#ifndef __NUCLEO__
#define __NUCLEO__


#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <termios.h>
#include <unistd.h>
#define NBligne 2
#define NBcolone 4
#define N 50


int usb_open(char const *);
int creationMatrice(int fd1,int fd2,FILE * fichier);


#endif
