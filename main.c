#include "structure.h"
#include "fichier.h"
#include "balle.h"
#include "izziredoublement.h"
#include <time.h>

int main(){
  Element * TBlock[NbL][NbC];
  Element * balle;
  int black[] = {0,0,0,255};
  int fd1 = usb_open("/dev/ttyACM0");
  int fd2 = usb_open("/dev/ttyACM1");
  FILE * fichier;
  int niveau=1;
  char skin[][50]={"Skin_balle/lv1.png","Skin_balle/lv2.png","Skin_balle/lv3.png","Skin_balle/lv4.png","Skin_balle/lv5.png","Skin_balle/lv6.png","Skin_balle/lv7.png","Skin_balle/lv8.png","Skin_balle/lv9.png"};
  dballe_t * structBalle;
  time_t temps1, temps2;//Les variables de temps sont de type time_t


  if(initAllSANDAL2(IMG_INIT_PNG)){
    puts("Failed to init SANDAL2");
    return -1;
  }

  if(createWindow(W_Window,H_Window,"Fenêtre",0,black,0)){
    puts("Failed to open the window");
    closeAllSANDAL2();
  }

  createImage(0,0,W_Window,H_Window,"Skin_balle/plateau_classe.png",0,2);

  temps1 = time(NULL);//On donne une valeur de temps à temps1.
  balle = init_balle(TBlock);
  getDataElement(balle,(void **)&structBalle);
  while(!PollEvent(NULL) && structBalle->run){
    fichier=fopen("matrice.txt","w");
    creationMatrice(fd1,fd2,fichier);
    fclose(fichier);
    fichier=fopen("ficdetest.txt","r");
    TabBlock(fichier,balle);
    fclose(fichier);
    updateWindow();
    displayWindow();
    SDL_Delay(16);
    clearTab(balle);
    temps2 = time(NULL);
    if(temps2-temps1>10){
      temps1 = time(NULL);
      niveau=(niveau+1)%NbSkin+1;
      setImageElement(balle,skin[niveau]);
      //printf("%d\n",niveau);
      if(structBalle->vx>0)
        structBalle->vx++;
      else if(structBalle->vx<0)
        structBalle->vx--;

      if(structBalle->vy>0)
        structBalle->vy++;
      else if(structBalle->vy<0)
        structBalle->vy--;
    }
  }
  //fclose(fichier);
  closeAllWindow();
  closeAllSANDAL2();


  return 0;
}
