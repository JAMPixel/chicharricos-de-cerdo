#ifndef __STRUCTURE__
#define __STRUCTURE__

#include <SANDAL2/SANDAL2.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define H_Window  700
#define W_Window  700
#define NbL 4
#define NbC 4
#define Lb  700/(fmin(NbL,NbC))
#define Xi 700*0.5
#define Yi 700*0.5
#define Vit 1
#define Vxi 1
#define Vyi 3
#define Rayon Lb*0.5
#define NbSkin 9

typedef struct{
  int rayon;
  int vx, vy;
  Element * tbloc[NbL][NbC];
  int run;
}dballe_t;

typedef struct structBlock{
  int type;
}structBlock_t;

#endif
